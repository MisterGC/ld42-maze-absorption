#include "fpscounter.h"
#include <QDateTime>
#include <QBrush>
#include <QPainter>

namespace mazabs
{
FpsCounter::FpsCounter(QQuickItem *parent): QQuickPaintedItem(parent), _currentFPS(0), _cacheCount(0)
{
    _times.clear();
    setFlag(QQuickItem::ItemHasContents);
}

FpsCounter::~FpsCounter()
{
}

void FpsCounter::recalculateFPS()
{
    qint64 currentTime = QDateTime::currentDateTime().toMSecsSinceEpoch();
    _times.push_back(currentTime);

    while (_times[0] < currentTime - 1000) {
        _times.pop_front();
    }

    int currentCount = _times.length();
    _currentFPS = (currentCount + _cacheCount) / 2;

    if (currentCount != _cacheCount) fpsChanged(_currentFPS);

    _cacheCount = currentCount;
}

int FpsCounter::fps()const
{
    return _currentFPS;
}

void FpsCounter::paint(QPainter*)
{
    recalculateFPS();
    update();
}
}
