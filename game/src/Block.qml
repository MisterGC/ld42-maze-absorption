import QtQuick 2.0
import Box2D 2.0

ImageBoxBody {
    id: theBlock
    source: "qrc:/gfx/block.png"
    density: 1
    friction: 0.3
    restitution: 0.5
    property bool isWall: true
    property var blackHole: null
    property bool centerEntered: false

    onBlackHoleChanged: {
        if (blackHole)
            bodyType = Body.Dynamic
    }

    Connections {
        target: blackHole
        onXChanged: updateVelocity()
        onYChanged: updateVelocity()
    }
    function updateVelocity() {
        var v = Qt.vector2d(blackHole.x - x + blackHole.width * 0.5,
                            blackHole.y - y + blackHole.height * 0.5)
        v = v.normalized()
        v = v.times(15. + Math.random() * 5)
        linearVelocity.x = v.x
        linearVelocity.y = v.y
    }

    onCenterEnteredChanged: {
        if (centerEntered) {
            bodyType = Body.Kinematic
            scale = 0.1
        }
    }
    Behavior on scale { NumberAnimation { duration: 1000 } }

    property real destructionTreshold: 0.11
    onScaleChanged: {
        if (scale < destructionTreshold)
            theBlock.destroy()
    }
}
