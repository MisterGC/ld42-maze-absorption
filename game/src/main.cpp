#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QScreen>
#include "levelmodel.h"
#include "fpscounter.h"

int main(int argc, char* argv[])
{
#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);
    qmlRegisterType<mazabs::FpsCounter>("maze.absorption", 1, 0, "FpsCounter");
    QQmlApplicationEngine engine;
    engine.addImportPath("plugins");
    auto size = QGuiApplication::screenAt(QPoint(0,0))->size();

    // TODO Reintro lazy loading again - currently it just loads the entire
    // world at once (is it really a bad thing?)
    mazabs::LevelModel model(65,
                             QPoint(size.width()/65, size.height()/65),
                             ":/gfx/world_map.png");
    engine.rootContext()->setContextProperty("worldModel", &model);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
