#ifndef LEVEL_MODEL_H
#define LEVEL_MODEL_H 

#include <vector>
#include <QObject>
#include <QVariantMap>
#include <QVariantList>
#include <QImage>
#include <QSet>

namespace mazabs
{

class LevelModel: public QObject
{
    Q_OBJECT
    Q_ENUMS(TileType)
    Q_PROPERTY(int playerPosX READ playerPosX WRITE setPlayerPosX NOTIFY playerPosXChanged)
    Q_PROPERTY(int playerPosY READ playerPosY WRITE setPlayerPosY NOTIFY playerPosYChanged)
    Q_PROPERTY(int columns READ columns NOTIFY columnsChanged)
    Q_PROPERTY(int rows READ rows NOTIFY rowsChanged)
    Q_PROPERTY(int tileSize READ tileSize NOTIFY tileSizeChanged)

public:
    LevelModel(int tileSize,
               QPoint tilesOnScreen,
               const QString& pathToLevel);

    enum TileType
    {
        TT_WALL,
        TT_FREE,
        TT_TRAP
    };

    int playerPosX() const;
    int playerPosY() const;
    int columns() const;
    int rows() const;
    int tileSize() const;
    QObject* worldObject() const;

public slots:
    // Load the entire world
    void loadWorld();
    void setPlayerPosX(int playerPosX);
    void setPlayerPosY(int playerPosY);

signals:
    void createTile(int pX, int pY);
    void playerPosXChanged(int playerPosX);
    void playerPosYChanged(int playerPosY);
    void columnsChanged(int columns);
    void rowsChanged(int rows);
    void tileSizeChanged(int tileSize);
    void worldObjectChanged(QObject* worldObject);

private:
    QSet<QString> loadedTiles_;
    QPoint tilesOnScreen_;
    QImage map_;
    int playerPosX_ = 0;
    int playerPosY_ = 0;
    int tileSize_ = 1;
    int m_columns;
    int m_rows;
    int m_tileSize;
};

}
#endif
