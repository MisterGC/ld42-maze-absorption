import QtQuick 2.0
import Box2D 2.0

ImageBoxBody {
    id: thePulse
    source: "qrc:/gfx/spaceship.png"
    density: 1
    friction: 0.3
    restitution: 0.5
    sensor: true
    bodyType: Body.Dynamic

    onBeginContact: {
        var entity = other.getBody().target
        if (entity.isPlayer)
            winTimer.start()
    }

    Timer
    {
        id: winTimer
        interval: 2000
        onTriggered: theSupervisor.screen = "win"
    }

}
