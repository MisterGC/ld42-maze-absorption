import QtQuick 2.0
import maze.absorption 1.0

FpsCounter{
    id: counter
    width: 200
    height: 100
    Text {
        anchors.centerIn: parent
        text: "FPS: " + counter.fps.toFixed(2)
        font.pixelSize: 20
        color: "white"
    }
}
