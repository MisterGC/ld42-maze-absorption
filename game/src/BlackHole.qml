import QtQuick 2.9
import Box2D 2.0

ImageBoxBody {
    id: theBlackHole

    source: "qrc:/gfx/blackhole.png"
    sensor: true
    density: 1
    friction: 0.3
    restitution: 0.5
    bodyType: Body.Dynamic
    property var player: null
    property int consumedBlocks: 0
    property int maxSpeed: theCircle.radius / 40

    Connections {
        target: player
        onXChanged: updateVelocity()
        onYChanged: updateVelocity()
    }

    function updateVelocity() {
        var v = Qt.vector2d(player.x - x, player.y - y)
        v = v.normalized()
        v = v.times(maxSpeed)
        linearVelocity.x = v.x
        linearVelocity.y = v.y
        effectiveRange.linearVelocity.x = v.x
        effectiveRange.linearVelocity.y = v.y
    }

    onBeginContact: {
        var entity = other.getBody().target
        if (entity.isWall){
            entity.centerEntered = true
            consumedBlocks++
        }
        else if (entity.isPlayer)
            entity.energy = 0
    }

    Rectangle {
         width: theCircle.radius * 2
         height: width
         color: "#baa7ea"
         radius: width*0.5
         z: -1
         opacity: 0.3
         anchors.centerIn: parent
         SequentialAnimation on opacity {
             loops: Animation.Infinite
             NumberAnimation { from: 0.3; to: 0.1; duration: 2000 }
             NumberAnimation { from: 0.1; to: 0.3; duration: 2000 }
         }
    }

    Body {
        id: effectiveRange
        target: theBlackHole
        bodyType: Body.Dynamic

        Circle {
            id: theCircle
            groupIndex: -1
            x: -0.5 * (radius + theBlackHole.width)
            y: x
            radius: theBlackHole.width + consumedBlocks
            sensor: true
            onBeginContact: {
                var entity = other.getBody().target
                if (entity.isWall)
                    entity.blackHole = theBlackHole
            }
        }
    }

    RotationAnimator on rotation {
        from: 0
        to: 360
        loops: Animation.Infinite
        duration: 10000
    }
}
