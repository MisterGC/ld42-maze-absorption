import QtQuick 2.0
import QtGamepad 1.0

Item {
    property alias dirControllerUsed: gamepad.buttonR3
    property real axisRightX: gamepad.axisRightX
    property real axisRightY: gamepad.axisRightY
    property real axisLeftX: gamepad.axisLeftX
    property real axisLeftY: gamepad.axisLeftY

    property bool anyButtonDown: gamepad.buttonA ||
                                 gamepad.buttonB ||
                                 gamepad.buttonL1 ||
                                 gamepad.buttonL2 ||
                                 gamepad.buttonR1 ||
                                 gamepad.buttonR2 ||
                                 gamepad.buttonStart ||
                                 gamepad.buttonSelect

    Connections {
        target: GamepadManager
        onGamepadConnected: gamepad.deviceId = deviceId
    }

    Gamepad {
        id: gamepad
        deviceId: GamepadManager.connectedGamepads.length > 0 ? GamepadManager.connectedGamepads[0] : -1
    }

}



