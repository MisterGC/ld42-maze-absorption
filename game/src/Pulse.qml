import QtQuick 2.0
import Box2D 2.0

ImageBoxBody {
    id: thePulse
    source: "qrc:/gfx/pulse.png"
    density: 1
    friction: 0.3
    restitution: 0.5
    sensor: true
    bullet: true
    bodyType: Body.Dynamic
    groupIndex: -1

    onBeginContact: {
        var entity = other.getBody().target
        if (entity.isWall) {
            if (entity.bodyType === Body.Dynamic) {
                entity.destroy()
                theWorld.blockExplosionFxAt(x, y)
            }
            else
                theWorld.impactAt(x, y)
        }
        thePulse.destroy()
    }

    Timer {
        running: true
        interval: 5000
        onTriggered: thePulse.destroy()
    }
}
