#include "levelmodel.h"
#include <QDebug>
#include <QRgb>

namespace mazabs
{

LevelModel::LevelModel(int tileSize,
                       QPoint tilesOnScreen,
                       const QString &pathToLevel):
    tilesOnScreen_(tilesOnScreen),
    map_(pathToLevel),
    tileSize_(tileSize)
{ }

int LevelModel::playerPosX() const
{
    return playerPosX_;
}

int LevelModel::playerPosY() const
{
    return playerPosY_;
}

void LevelModel::setPlayerPosX(int playerPosX)
{
    if (playerPosX_ == playerPosX)
        return;

    playerPosX_ = playerPosX;
    emit playerPosXChanged(playerPosX_);
}

void LevelModel::setPlayerPosY(int playerPosY)
{
    if (playerPosY_ == playerPosY)
        return;

    playerPosY_ = playerPosY;
    emit playerPosYChanged(playerPosY_);
}

void LevelModel::loadWorld()
{
   loadedTiles_.clear();

   QList<QPoint> included;
   for (int x=0; x < map_.width(); ++x)
       for (int y=0; y < map_.height(); ++y)
       {
           if (qRed(map_.pixel(x,y)) < 10)
               included << QPoint(x,y);
       }

   for (auto& m: included){
       auto id = QString("w_%1_%2").arg(m.x()).arg(m.y());
       if (!loadedTiles_.contains(id)) {
           loadedTiles_.insert(id);
           emit createTile(m.x() * tileSize(),
                           m.y() * tileSize());
       }
   }
}

int mazabs::LevelModel::columns() const
{
    return map_.width();
}

int LevelModel::rows() const
{
    return map_.height();
}

int LevelModel::tileSize() const
{
    return tileSize_;
}

}
