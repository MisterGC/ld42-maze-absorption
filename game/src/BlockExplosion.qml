import QtQuick 2.0
import QtQuick.Particles 2.0

Item {
    id: theEffect
    Component.onCompleted: emitter.burst(10)

    ParticleSystem { id: particleSystem }

    Emitter {
        id: emitter
        enabled: false
        anchors.right: parent.right
        anchors.top: parent.top
        system: particleSystem
        emitRate: 5
        lifeSpan: 1000
        sizeVariation: 20
        velocity: AngleDirection {
            angleVariation: 360
            magnitude: 200
        }
    }

    ImageParticle {
        source: "qrc:/gfx/blockpart.png"
        system: particleSystem
    }

    Timer {
        id: fxDestruct
        interval: 1000
        running: true
        onTriggered: { theEffect.destroy() }
    }
}
