import QtQuick 2.0
import Box2D 2.0
import QtMultimedia 5.9


ImageBoxBody {
    id: thePlayer
    source: "qrc:/gfx/player.png"
    groupIndex: -1
    property bool isPlayer: true
    property int energy: 100
    property int maxSpeed: 30
    onEnergyChanged: {
        if (energy <= 0)
            theSupervisor.screen = "fail"
    }


    SoundEffect {
        id: gotHitSound
        source: "qrc:/sfx/hit.wav"
    }

    onBeginContact: {
        var entity = other.getBody().target
        if (other.getBody().bodyType === Body.Dynamic && entity.isWall) {
            gotHitSound.play()
            energy -= 5
            entity.destroy()
            theWorld.blockExplosionFxAt(x, y)
        }
    }

    function sqrdSpeed() {
        return linearVelocity.x * linearVelocity.x
                + linearVelocity.y * linearVelocity.y
    }


    Item {
        id: laserGun
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        width: 10
        height: 10
    }

    Item {
        id: laserGun2
        anchors.centerIn: parent
        width: 10
        height: 10
    }

    SoundEffect
    {
        id: laserSound
        source: "qrc:/sfx/lasershoot.wav"
    }

    Timer {
        interval: 150
        running: true
        repeat: true
        onTriggered: {
            var rotRad = thePlayer.rotation * Math.PI / 180.
            var vel = Qt.vector2d(Math.cos(rotRad), Math.sin(rotRad))
            vel = vel.times(50/vel.length())

            var pos = thePlayer.parent.mapFromItem(laserGun, laserGun.x, laserGun.y)
            var obj = pulseComp.createObject(thePlayer.parent, {
                                                 "x": pos.x,
                                                 "y": pos.y,
                                                 "rotation": thePlayer.rotation})
            obj.linearVelocity.x = vel.x
            obj.linearVelocity.y = vel.y

            var pos2 = thePlayer.parent.mapFromItem(laserGun2, laserGun2.x, laserGun2.y)
            var obj2 = pulseComp.createObject(thePlayer.parent, {
                                                 "x": pos2.x,
                                                 "y": pos2.y,
                                                 "rotation": thePlayer.rotation})
            obj2.linearVelocity.x = vel.x
            obj2.linearVelocity.y = vel.y
            laserSound.play()
        }

    }

    Component {
        id: pulseComp
        Pulse { }
    }

    bodyType: Body.Dynamic
    fixedRotation: true
    density: 1
    friction: 0.3
    restitution: 0.5
}
