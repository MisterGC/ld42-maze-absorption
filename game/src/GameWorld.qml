import QtQuick 2.0
import Box2D 2.0
import QtMultimedia 5.9

Flickable {
    id: theWM
    contentWidth: theItem.width
    contentHeight: theItem.height
    boundsBehavior: Flickable.StopAtBounds
    property variant physicsWorld: thePhysicsWorld

    property int hMargin: width/2
    property int vMargin: height/2

    property var player: null

    Component.onCompleted: {
        worldModel.loadWorld()
    }

    Connections {
        target: player
        onXChanged: advanceCanvas()
        onYChanged: advanceCanvas()
        ignoreUnknownSignals: true
    }

    Connections {
        target: worldModel
        onCreateTile: {
            theBlockComp.createObject(theWM.contentItem, {"x": pX, "y": pY})
        }
    }

    Component {
        id: theBlockComp
        Block {}
    }

    Component {
        id: explosionFxComp
        BlockExplosion {}
    }

    SoundEffect {
        id: explosionSound
        source: "qrc:/sfx/blockexploding.wav"
    }

    function blockExplosionFxAt(x, y) {
        var fx = explosionFxComp.createObject(contentItem, {"x": x, "y": y})
        explosionSound.play()
    }

    SoundEffect {
        id: impactSound
        source: "qrc:/sfx/pulsehitsolidwall.wav"
    }

    function impactAt(x, y) {
        impactSound.play()
    }

    function advanceCanvas()
    {
        var pos = {"x": player.x, "y": player.y}
        var pContainer = theWM

        var notAtBegOrEndOfLevelX = (pos.x >= hMargin) && ((pos.x + player.width) <= (pContainer.contentWidth - hMargin));
        var hitsRight = (pos.x >= (pContainer.contentX + pContainer.width - hMargin));
        var hitsLeft = (pos.x <= (pContainer.contentX + hMargin));
        var moveScreenX = notAtBegOrEndOfLevelX && (hitsLeft || hitsRight)
        if (moveScreenX)
        {
            var deltaX = pos.x - pContainer.contentX;
            deltaX -= hitsLeft ? hMargin : (pContainer.width - hMargin);
            pContainer.contentX += deltaX;
        }

        var notAtBegOrEndOfLevelY = (pos.y >= vMargin) && ((pos.y + player.height) <=
                                                         (pContainer.contentHeight-vMargin));
        var hitsTop = (pos.y <= (pContainer.contentY + vMargin));
        var hitsBottom = (pos.y >= (pContainer.contentY + pContainer.height - vMargin));
        var moveScreenY = notAtBegOrEndOfLevelY && (hitsBottom || hitsTop)
        if (moveScreenY)
        {
            var deltaY = pos.y - pContainer.contentY;
            deltaY -= hitsTop ? vMargin : (pContainer.height - vMargin)
            pContainer.contentY += deltaY;
        }
    }

    Rectangle{
        id: theItem
        color: "#2f2f2d"
        width: worldModel.tileSize * worldModel.columns
        height: worldModel.tileSize * worldModel.rows

        World {
            id: thePhysicsWorld
            gravity: Qt.point(0.,0.)
        }
    }
}
