import QtQuick 2.0

Rectangle {
    color: "#2f2f2d"
    Image {
        source: "qrc:/gfx/title.png"
        anchors.fill: parent
    }

    MouseArea {
        anchors.fill: parent
        onClicked: theSupervisor.screen = "game"
    }
}
