import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Window 2.2
import Box2D 2.0

ApplicationWindow {
    title: qsTr("Maze Absorption")
    visible: true
    visibility: Window.Maximized

    Item {
        id: theSupervisor
        anchors.fill: parent

        property string screen: "title"
        onScreenChanged: {
            switch(screen) {
            case "title": theLoader.sourceComponent = theTitle; break;
            case "game": theLoader.sourceComponent = theRealGame; break;
            case "win": theLoader.sourceComponent = theWin; break;
            case "fail": theLoader.sourceComponent = theFailure; break;
            }
        }

        FpsCounter {
            x: 10
            y: 10
            z: 10
        }

        Loader {
            id: theLoader
            focus: true
            anchors.fill: parent
            sourceComponent: theTitle
        }

        Component {
            id: theTitle
            Title {
                GameController {
                    anchors.fill: parent
                    onAnyButtonDownChanged: {
                        if (anyButtonDown)
                            theSupervisor.screen = "game"
                    }
                }
            }

        }

        Component {
            id: theWin
            YouDidIt {
                GameController {
                    anchors.fill: parent
                    onAnyButtonDownChanged: {
                        if (anyButtonDown)
                            theSupervisor.screen = "title"
                    }
                }
            }
        }

        Component {
            id: theFailure
            YouFailed {
                GameController {
                    anchors.fill: parent
                    onAnyButtonDownChanged: {
                        if (anyButtonDown)
                            theSupervisor.screen = "game"
                    }
                }
            }
        }

        Component {
            id: theRealGame

            Item {
                anchors.fill: parent

                Rectangle {
                    z: 99
                    opacity: 0.9
                    y: 10
                    x: 10
                    height: parent.height / 30
                    width: 10 * height
                    color: "#a0cfe6"
                    Rectangle {
                        anchors.centerIn: parent
                        width: 0.95 * parent.width * (player.energy/100)
                        height: 0.95 * parent.height
                        color: "#2b7aa0"
                    }
                }

                GameWorld{
                    id: theWorld
                    anchors.fill: parent
                    player: player

                    GameController {
                        anchors.fill: parent

                        onDirControllerUsedChanged: {
                            if (!dirControllerUsed) {
                                player.linearVelocity.x = 0
                                player.linearVelocity.y = 0
                            }
                        }

                        function updatePlayerFacing() {
                            var v = Qt.vector2d(axisRightX,axisRightY)
                            if (v.length() > 0.01) {
                                var angle= Math.atan2(v.y, v.x);
                                player.rotation = (angle / Math.PI) * 180;
                            }
                        }

                        onAxisRightXChanged: updatePlayerFacing()
                        onAxisRightYChanged: updatePlayerFacing()

                        onAxisLeftXChanged: {
                            if (Math.abs(axisLeftX) > 0.2)
                                player.linearVelocity.x = axisLeftX * player.maxSpeed;
                            else
                                player.linearVelocity.x = 0
                        }
                        onAxisLeftYChanged: {
                            if (Math.abs(axisLeftY) > 0.2)
                                player.linearVelocity.y = axisLeftY * player.maxSpeed;
                            else
                                player.linearVelocity.y = 0
                        }
                    }

                    Player {
                        id: player
                        parent: theWorld.contentItem
                        x: 3 * worldModel.tileSize
                        y: 3 * worldModel.tileSize
                    }

                    DebugDraw {
                        visible: false
                        parent: theWorld.contentItem
                        world: theWorld.physicsWorld
                        anchors.fill: parent
                    }


                    BlackHole {
                        parent: theWorld.contentItem
                        player: player
                        x: 750
                        y: 750
                    }

                    SpaceShip {
                        parent: theWorld.contentItem
                        anchors.right: parent.right
                        anchors.rightMargin: width/3
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: height/3
                    }

                }
            }
        }
    }
}
